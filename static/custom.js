let timer;

const copyPath = event => {
    const el = event.target.parentNode.parentNode.querySelector(`.copy`)
    const done = event.target.parentNode.parentNode.querySelector(`.done`)

    if (el.classList.contains('flip')) {
        return
    }

    timer && clearTimeout(timer)

    event.preventDefault()

    try {
        navigator.clipboard.writeText(event.target.parentNode.value)
    } catch (err) {
        console.error(err)
    }

    el.classList.add(`flip`)
    done.classList.add(`flip`)

    setTimeout(() => {
        el.classList.remove(`flip`)
        done.classList.remove(`flip`)
    }, 1000);
}

const showCopyBtn = event => {
    const el = event.target.querySelector('.copyButton')

    if (!el) {
        return
    }

    el.classList.replace('hidden', 'visible');
}

const hideCopyBtn = event => {
    const el = event.target.querySelector('.copyButton')

    if (!el) {
        return
    }

    if (event.target.querySelector('.done.flip')) {
        const wait = setInterval(() => {
            if (!event.target.querySelector('.done').classList.contains('flip')) {
                clearInterval(wait)
                event.target.querySelector('.copyHint').classList.replace('hintVisible', 'hintHidden')
                timer && clearTimeout(timer)
                el.classList.replace('visible', 'hidden');
            }
        }, 100);
    } else {
        event.target.querySelector('.copyHint').classList.replace('hintVisible', 'hintHidden')
        timer && clearTimeout(timer)
        el.classList.replace('visible', 'hidden');
    }
}

const showCopyHint = event => {
    timer = setTimeout(() => {
        const el = event.target.parentNode.parentNode.querySelector('.copyHint')
        
        if (el.classList.contains('hintActive')) {
            return
        }
        
        el.classList.replace('hintHidden', 'hintVisible')
    }, 500)
}

const hideCopyHint = event => {
    const el = event.target.parentNode.parentNode.querySelector('.copyHint')

    el.classList.replace('hintVisible', 'hintHidden')
}

const genDetails = async event => {
    let path, details

    if (event.target.tagName === 'P') {
        path = event.target.parentNode.attributes.value.value
        details = event.target.parentNode.parentNode.parentNode
    } else {
        path = event.target.attributes.value.value
        details = event.target.parentNode.parentNode
    }

    if (details.childNodes.length > 1) {
        return
    }

    const span = new DOMParser().parseFromString(`<svg class="loadingIcon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" width=20 height=20>` +
                                                    `<circle cx=10 cy=10 r=8 fill="none" stroke="black" stroke-width="2px" stroke-linecap="round" stroke-dasharray="30 5 15" style="transform-origin: 10px 10px">` +
                                                        `<animate dur="2s" repeatCount="indefinite" attributeName="stroke-dasharray" values="30 5 15; 5 100 15; 30 5 150" keyTimes="0; 0.5; 1"/>` +
                                                        `<animateTransform dur="0.5s" repeatCount="indefinite" attributeType="XML" attributeName="transform" type="rotate" from="0 0 0" to="360 0 0"/>` +
                                                    `<circle>` +
                                                `</svg>`, 'text/html')

    details.appendChild(span.body.childNodes[0])

    const resp = await fetch(path)
    const content = new DOMParser().parseFromString(await resp.text(), `text/html`)

    details.removeChild(details.querySelector(`.loadingIcon`))
    content.body.childNodes.forEach(node => {
        details.appendChild(node)
    })
}